# CS4033_AI

Artificial Intelligence at the University of Cincinnati CS4033 Spring 2019

- [CS4033_AI](#cs4033ai)
  - [Repository](#repository)
  - [FRIL Resources](#fril-resources)
    - [Debugging](#debugging)
  - [License](#license)

## Repository

This repository contains the work for some of the homeworks for the Spring 2019 semester of CS4033 AI with Professor Ralescu. This repository also contains the FRIL executable interpreter `fril4984.exe` in [/exec/](/exec/).

## FRIL Resources

The following may or may not be helpful. Documentation of the FRIL language is difficult to find as the language has been around for a while and was made proprietary in 1986[^1].

https://eecs.ceas.uc.edu/~aralescu/323Fall2005/LECTURES/Fril_index.html

https://en.wikipedia.org/wiki/Fril

https://www.cs.cmu.edu/afs/cs/Web/Groups/AI/areas/fuzzy/com/fril/fril.txt

https://sorrell.github.io/2012/01/01/FRIL-Tutorial.html

https://sorrell.github.io/files/Fril.pdf

Fril Manual in the Internet Archive:
https://web.archive.org/web/20120415063330/http://www.enm.bris.ac.uk/ai/martin/FrilManual/index.html

Local version of FRIL manual: [/man/frilmanual.html](/man/frilmanual.html)

Random Fril lecture from UC:
https://eecs.ceas.uc.edu/~aralescu/323Fall2005/LECTURES/Lecture1.html

### Debugging

For debugging in FRIL follow the instructions in Nick Sorrell's Fril Tutorial document here: [https://sorrell.github.io/files/Fril.pdf]. You will need to use the `fr_trace.frm` file in the [/exec/](/exec/) directory of this repo.

## License

This entire repository is for educational purposes only.

[^1]: https://www.cs.cmu.edu/afs/cs/Web/Groups/AI/areas/fuzzy/com/fril/fril.txt