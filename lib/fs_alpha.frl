/* *******************************************************
File: fs_alpha.frl	     Version: 4.1    Date: 25/3/88

Copyright: Equipu-AIR, Ltd.

Summary: This file comprises a set of useful programming
utilities including a controlled repeat ... fail loop in
procedure "for", and three procedures "ac", "var_convert"
and "replace_allvars" for replacing terms with new
variable patterns.

Goal (for +<start> +<finish> -<index>) is used like repeat
in conjunction with fail.  It simulates the for ... next
construct of traditional procedural languages by
repeatedly backtracking.  Initially, <index> is bound to
<start> and each time "for" is resatisfied, <index> is
incremented by one until it is equal to <finish>, after
which "for" fails.  Hence both <start> and <finish> must
be integers, the former no larger than the latter.

Goal (ac +<term1> -<term2>) effectively generates <term2>
as a copy of <term1> with each instance of a variable in
<term1> replaced by a new variable in <term2>.	Variables
which share in <term1> also share in <term2>, but the
variables in <term2> are completely isolated from those
in <term1>.

Goal (var_convert +<list1> -<list2>) succeeds with each
top-level element of <list1> replaced by a new variable
in <term2>.

Goal (replace_allvars +<goal1> -<goal2>) uses
"var_convert" to replace the arguments of <goal1> by new
variables in <goal2>.

USERs: ac/2, for/3

EXPORTs: var_convert/2, replace_allvars/2

INTERNALs: ac1/6, is_in/4

TEMPORARYs: none

IMPORTs: none

******************************************************* */

((ac X Y)
  (ac1 X () () _ _ Y))

((ac1 X V N V N R)
  (var X)
  (is_in X V N R)
  (!))
((ac1 X V N (X|V) (R|N) R)
  (var X)
  (!))
((ac1 X V N V N X)
  (atomic X)
  (!))
((ac1 (H|T) V N R R1 (X|Y))
  (ac1 H V N V1 N1 X)
  (ac1 T V1 N1 R R1 Y)
  (!))
((ac1 (P|A) V N R R1 (P|A2))
  (ac1 A V N R R1 A2))

((is_in X (H|T) (A|B) A)
  (stricteq X H)
  (!))
((is_in X (_|T) (_|Y) R)
  (is_in X T Y R))

((for F F F)
  (!))
((for S F S))
((for S F X)
  (sum S 1 N)
  (for N F X))

((var_convert X X)
  (var X)
  (!))
((var_convert () ()))
((var_convert (_|T) (_|T2))
  (var_convert T T2))

((replace_allvars (G|X) (G|Y))
  (var_convert X Y))

