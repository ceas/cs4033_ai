/* *******************************************************
File: fs_shell.frl	     Version: 4.1    Date: 25/3/88

Copyright: Equipu-AIR, Ltd.

Summary: The Fril support logic shell is described in
detail in chapter 9 of the Fril manual.  The shell is
invoked using the predicate "fs".  The shell may also be
invoked when using the Fril prolog tracer, if the trace
encounters a "supp_query" goal.  Predicates "collect_subs"
"diction_parents" and "find_parents" are concerned with
finding parent and child relations of predicates in the
database, which are then added to the database in the
form of Fril relations "parent" and "child".

Goal (fs +<term>) invokes a support logic shell with a
"current goal" defined in terms of <term>.  If <term> is a
goal pattern which matches in the knowledge base, this
becomes the current goal.  If <term> is a constant symbol
which names a user predicate currently defined in the
knowledge base, then the current goal becomes a most
general goal involving this predicate name.  The shell
provides a elementary but user friendly environment for
the exploration of support logic knowledge bases.  The
whole of the Fril language is available to the user within
the shell, but in addition the user can select from a
variety of single symbol and two symbol inputs to request
options.  For example option "q" finds all solutions with
supports for the current goal, whereas "re <file>" reloads
file named <file>.  The complete set of options available
to the user is displayed using the help menu option "h".

Goal (collect_subs +<goal>) establishes the parent and child
relations of <goal> and all of its sub-goals.  Fril
relations "parent" and "child" are created with tuples of
arity 5 and 3 respectively, as follows:

Goal (parent <name> <type> <arity> <num> <child_list>)
succeeds if <name> is the name of parent procedure; <type>
is its type which is "relation", "dempster", "clause" or
"undefined";  <arity> is the arity of parent procedure
<name> which is a non-negative integer or the symbol "n"
if the arity is indeterminate;	<num> is the number of
clauses of parent procedure <name> with arity <arity>;
and <child_list> is a list (possibly empty) of list-pairs
of the form (<child_name> <child_arity>) where
<child_name> is the name of a child sub-goal of parent
procedure <name>, and <child_arity> is the number of its
sub-goal arguments.  Note that the <type> "undefined"
corresponds to the case that a sub-goal predicate has not
been defined or loaded into the knowledge base.

Goal (child <name> <arity> <parent_arg>) succeeds if
<name> is the name of the child procedure;  <arity> is the
arity of the child procedure; and <parent_arg> is either
the symbol "top_level" if the child procedure <name> has
no parent, or it is a non-empty list of list-pairs of the
form (<parent_name> <parent_arity>) where <parent_name> is
the name of a parent procedure of child procedure <name>,
and <parent_arity> is the number of parent procedure
arguments.

Goal (diction_parents) establishes the parent and child
relations of all predicates consulted or reconsulted as
determined by the dictionary predicate "diction" (file
fs_load.frl).

Goal (find_parents) establishes the parent and child
relations of all predicates in the database.  This could
include all shell procedures as well as the knowledge base
which is of interest - and so the predicate "find_parents"
should be used with circumspection.

USERs: fs/1

EXPORTs: collect_subs/1, diction_parents/0,
	 find_parents/0

INTERNALs: fril_shell/2, print_current_goal/1,
	   interpret_input/2, interpret_ipb/4,
	   interpret_input0/4, interpret_input2/4,
	   interpret_input1/2, q_fs/1, q1_fs/1,
	   print_goals/0, help_menu/1, select_body/2,
	   select_body1/3, plist_select/1, plist_sel/1,
	   prolog_meta/1, support_meta/1,
	   support_children/3, support_child/3,
	   prolog_children/3, prolog_child/3, type/2,
	   arb_length/2, assert_arity/4, assert_parents/2,
	   assert_parent/4, assert_child_name/2,
	   assert_children/0, tidy_parent_child/0,
	   convert_dict/0, convert_diction/0,
	   convert_diction1/1, assert_temp_diction1/0,
	   assert_temp_diction2/1, collect_subgoals/0,
	   find_parent/1

TEMPORARYs: temporary_goal_select/n, temp_child/4,
	    temp_arity/4, temp_parent/4, parent/5,
	    child/3, temp_diction/1, temp_diction1/1

IMPORTs:
File fs_peekc.frl: peekc/1
File fs_addcl.frl: interp_addcl/2
File fs_alpha.frl: ac/2, replace_allvars/2
File fs_list.frl: listf/n, listp/n, listx/n, listi/n,
		  cl_col/4, cl_body/4, list_support/2
File fs_goal.frl: fact/2, rfact/2, rule/2, demp/2

******************************************************* */

reload fs_peekc
reload fs_addcl
reload fs_alpha
reload fs_list
reload fs_goal

((fs X)
  (errm n)
  (repeat)
  (kb_garbage)
  (flush stdin)
  (fril_shell () X)
  (kb_garbage)
  (errm y))

((fril_shell L ((H|T)|B))
  (!)
  (p Cannot treat a goal list at present - '')
  (p taking first goal (H|T) only)
  (pp) (pp)
  (fril_shell L (H|T)))
((fril_shell L (H|T))
  (pp)
  (print_current_goal (H|T)) (pp)
  (kb_garbage)
  (flush stdin)
  (p Shell level (H|L)) (pp)
  (p Option ? "(h for help): ")
  (reset_pause_count stdout)
  (!)
  (interpret_input (H|L) (H|T)))
((fril_shell L K)
  (dict K)
  (cl ((K|T)|B))
  (replace_allvars (K|T) (K|S))
  (!)
  (fril_shell L (K|S)))
((fril_shell L A)
  (pp)
  (p Fril shell expecting a goal or predicate '')
  (p name as argument) (pp)
  (p Enter appropriate argument "(or a to abort): ")
  (r X)
  (!)
  (if (eq X a) ((abort)) ((fril_shell L X))))

((print_current_goal G)
  (p Current goal is G) (pp))

((interpret_input L G)
  (repeat)
  (peek stdin W)
  (r X)
  (interpret_ipb L X W G))

((interpret_ipb L b _ G)
  (peekb stdin W)
  (eq W 10)
  (!)
  (pp)
  (p Completed investigations on goal G) (pp)
  (p Backing up a "level:") (pp))
((interpret_ipb L X W G)
  (peekc Y)
  (if (eq W 40)
		((interp_addcl X Y))
		((interpret_input0 L X Y G)))
  (kb_garbage)
  (flush stdin)
  (p Shell level L) (pp)
  (p Option ? "(h for help): ")
  (reset_pause_count stdout)
  (!)
  (fail))

((interpret_input0 L X 10 G)
  (!)
  (interpret_input1 X G))
((interpret_input0 L X Y G)
  (r Z)
  (interpret_input2 L X Z G))

((interpret_input2 L s Y G)
  (!)
  (int Y)
  (snips select_body G Y))
((interpret_input2 L lo Y G)
  (!)
  (load Y)
  (pp)
  (p File Y loaded) (pp)
  (pp))
((interpret_input2 L re Y G)
  (!)
  (reload Y)
  (pp)
  (p File Y reloaded) (pp)
  (pp))
((interpret_input2 L r Y G)
  (!)
  (if (dict reconsult) () ((p 'Loading fs_load...')
								(load fs_load)
							(pp)))
  (reconsult Y)
  (pp)
  (p File Y reconsulted and dictionary summary '')
  (p installed)
  (pp) (pp))
((interpret_input2 L c Y G)
  (!)
  (if (dict consult) () ((p 'Loading fs_load...')
							  (load fs_load)
						      (pp)))
  (consult Y)
  (pp)
  (p File Y consulted and dictionary summary '')
  (p installed)
  (pp) (pp))
((interpret_input2 L fs Y G)
  (!)
  (fril_shell L Y)
  (print_current_goal G) (pp))
((interpret_input2 L X Y G)
  (pp)
  (if (X Y) () ((pp ?)))
  (pp))

((interpret_input1 a G)
  (!)
  (pp)
  (p Aborting and returning to Fril top-level) (pp)
  (abort))
((interpret_input1 h G)
  (!)
  (help_menu G))
((interpret_input1 v G)
  (!)
  (pp_count stdout)
  (escape stdout
	 (p Verifying the parent and child relations)
	 (p '' of subgoals of G))
  (pp_count stdout)
  (collect_subs G)
  (print_goals))
((interpret_input1 w G)
  (!)
  (pp_count stdout)
  (if (dict diction)
		((escape stdout (p Verifying the parent and child relations)
				(p '' of all '(re)consulted' predicates))
		 (pp_count stdout)
		 (diction_parents)
		 (print_goals))
		((escape stdout (pq Predicate dictionary "diction" not installed)
				(p '' - possibly no '(re)consult' done yet))
		 (pp_count stdout)
		 (escape stdout (p Option v being invoked instead))
		 (pp_count stdout) (pp_count stdout)
		 (interpret_input1 v G))))
((interpret_input1 p G)
  (!)
  (pp)
  (print_current_goal G) (pp))
((interpret_input1 q G)
  (!)
  (pp_count stdout)
  (escape stdout (p Finding all solutions to goal G))
  (pp_count stdout) (pp_count stdout)
  (q_fs (G))
  (pp))
((interpret_input1 s G)
  (!)
  (eq G (H|X))
  (ac (H|X) (H|Z))
  (snips orr ((rule (H|Z) (_ N)))
		 ((demp (H|Z) (_ N)))
		 ((eq N 1)))
  (cl ((H|Z)|B) 1 I)
  (snips select_body G I)
  (escape stdout (pause_msg))
  (kb_garbage)
  (escape stdout (eq N I))
  (pp))
((interpret_input1 l (G|X))
  (!)
  (pp_count stdout)
  (p Listing of predicate G in standard format)
  (pp_count stdout)
  (list_pred (G|X))
  (pp))
((interpret_input1 f (G|X))
  (!)
  (pp_count stdout)
  (p Listing of predicate G showing itype definitions)
  (pp_count stdout)
  (list_fuz (G|X))
  (pp))
((interpret_input1 g G)
  (!)
  (pp_count stdout)
  (p Listing of goal G)
  (pp_count stdout)
  (list_goal G)
  (pp))
((interpret_input1 i G)
  (!)
  (pp_count stdout)
  (if (idict X)
		((p Listing of all current itype definitions)
		 (pp_count stdout)
		 (listi _))
		((p There are no itypes currently defined) (pp)))
  (pp))
((interpret_input1 k G)
  (!)
  (if (cdict diction)
		((killd all)
	 (pp)
	 (p All predicates loaded using consult or '')
	 (p reconsult 'DESTROYED'))
		((pp)
		 (p No action taken - no '(re)consulted' predicates to kill)))
  (pp) (pp))
((interpret_input1 d G)
  (!)
  (pp_count stdout)
  (if (cdict diction)
		((p Dictionary summary of predicates '')
	 (p consulted or reconsulted)
	 (pp_count stdout) (pp_count stdout)
	 (escape stdout (pd)))
		((p Dictionary not found - there are no '(re)consulted')
		 (p '' predicates to display) (pp)))
  (pp))
((interpret_input1 e G)
  (p Are you sure you wish to exit Fril '')
  (p altogether ? '(y/n) : ')
  (r X)
  (pp)
  (if (eq X y) ((exit 0)) ()))
((interpret_input1 X G)
  (p Option X unknown - type h for option menu)
  (pp))

((q_fs X)
  (q1_fs X))
((q_fs _))

((q1_fs (X))
  (!)
  (supp_query (X) (L U))
  (p X (L U))
  (pp)
  (pause_check stdout)
  (failing stdout))
((q1_fs X)
  (supp_query X (L U))
  (p X (L U))
  (pp)
  (pause_check stdout)
  (failing stdout))

((print_goals)
	(list_fuz parent)
	(pause_msg)
	(list_fuz child)
	(pp))

((help_menu (G|X))
  (pp)
  (p You may type any command including adding to '')
  (p the "database,") (pp)
  (p '     ' just as if you were at Fril '')
  (p "top-level, viz. :") (pp)
  (p Fril >) (pp)
  (p In particular you may invoke the "FRIL" shell '')
  (p on some "<sub-goal>") (pp)
  (p '     ' by "typing:    " fs "<sub-goal>") (pp)
  (p In addition the following options are "available:")
  (pp) (pp)
  (p h - prints this help menu) (pp)
  (p q - finds all solutions to the current goal (G|X))
  (pp)
  (p g - prints a listing of the current goal (G|X))
  (pp)
  (p l - lists the predicate G in standard format) (pp)
  (p f - lists the predicate G showing itype '')
  (p definitions) (pp)
  (p b - terminates the current shell normally and '')
  (p backs up a level) (pp)
  (p a - aborts to Fril top-level) (pp)
  (p e - exits from Fril altogether after confirmation)
  (pp)
  (p v - verifies the current goal (G|X) printing the '')
  (p parent and) (pp)
  (p '          ' child relations of its subgoals) (pp)
  (p w - verifies the parent and child relations of all '')
  (p '(re)consulted') (pp)
  (p '          ' predicates) (pp)
  (p p - prints the current goal (G|X)) (pp)
  (pp)
  (pause_msg)
  (escape stdout (p s - evaluates both body and clause for each '')
		 (p of the clauses)) (pp_count stdout)
  (escape stdout (p '          ' of goal (G|X))) (pp_count stdout)
  (escape stdout (p i - lists all current itype definitions))
  (pp_count stdout)
  (escape stdout (p k - kills all predicates loaded using consult '')
		 (p or reconsult)) (pp_count stdout)
  (escape stdout (p d - prints dictionary summary of predicates loaded))
  (pp_count stdout)
  (escape stdout (p '          ' using consult or reconsult))
  (pp_count stdout)
  (escape stdout (p r "<file>" - reconsults "<file>" with dictionary '')
		 (p summary)) (pp_count stdout)
  (escape stdout (p c "<file>" - consults "<file>" with dictionary '')
		 (p summary)) (pp_count stdout)
  (escape stdout (p s "<n>" - selects the "<n>th" clause of goal (G|X)))
  (pp_count stdout)
  (escape stdout (p '          ' and evaluates both body and clause))
  (pp_count stdout)
  (escape stdout (p lo "<file>" - loads "<file>")) (pp_count stdout)
  (escape stdout (p re "<file>" - reloads "<file>")) (pp_count stdout)
  (pp_count stdout)
  (escape stdout (pause_msg))
  (pp))

((select_body (G|_) _)
  (cdict G)
  (orr ((fact (G|_) _)) ((rfact (G|_) _)))
  (!)
  (pp)
  (p Select option is not relevant for facts or '')
  (p relations)
  (pp) (pp))
((select_body (G|X) N)
  (cdict G)
  (!)
  (ac X Y)
  (select_body1 (G|Y) N rule))
((select_body (G|X) N)
  (ddict G)
  (!)
  (ac X Y)
  (select_body1 (G|Y) N demp))
((select_body (G|_) _)
  (pp)
  (p Failing to select G - and failing)
  (pp)
  (fail))

((select_body1 (G|X) N FLAG)
  (cl ((G|X)|B) S N N)
  (!)
  (pp_count stdout)
  (if (eq FLAG rule)
	((escape stdout (p Clause N of rule G 'is:')))
	((escape stdout (p Clause N of dempster G 'is:'))))
  (pp_count stdout) (pp_count stdout)
  (escape stdout (p '(') (p (G|X)))
  (cl_body stdout B 3 sym)
  (escape stdout (p))
  (list_support stdout S)
  (pp_count stdout)
  (pause_check stdout)
  (kill temporary_goal_select)
  (findall (B S1) ((supp_query B S1)
		   (addcl ((temporary_goal_select|X) (true S1)) : S))
		  L)
  (if (eq FLAG rule)
	((escape stdout (p Inferences from body of '')
				(p clause N of G 'are:')))
	((escape stdout (p Inferences from body of '')
				(p clause N of dempster G 'are:'))))
  (pp_count stdout) (pp_count stdout)
  (pause_check stdout)
  (plist_select L)
  (pp_count stdout)
  (pause_check stdout)
  (escape stdout (findall ((G|X2) S2)
			((supp_query ((temporary_goal_select|X2)) S2)) L1))
  (kill temporary_goal_select)
  (if (eq FLAG rule)
	((escape stdout (p Inferences from clause N of G 'are:')))
	((escape stdout (p Inferences from clause N of '')
			(p dempster G 'are:'))))
  (pp_count stdout) (pp_count stdout)
  (pause_check stdout)
  (plist_sel L1))

((plist_select ((G S)|R))
  (escape stdout
	 (pspaces 6)
	 (cl_col stdout G 7 sym)
	 (p '' : S))
  (pp_count stdout)
  (pause_check stdout)
  (!)
  (plist_select R))
((plist_select ()))

((plist_sel ((G S)|R))
  (escape stdout
     (pspaces 6)
	 (wrq stdout G)
	 (p '' : S))
  (pp_count stdout)
  (pause_check stdout)
  (!)
  (plist_sel R))
((plist_sel ()))

(prolog_meta
	(neg) (negg) (orr) (?) (supp_query) (findall) (isall)
	(forall) (if) (snips) (qh) (wh) (oh) (qs) (ws) (os))

(support_meta
	(not) (or) (and) (?))

((support_children X A ((H|T)|B))
	(support_child X A (H|T))
	(!)
	(support_children X A B))
((support_children X A ()))

((support_child X _ (H|_))
	(stricteq X H)
	(!))
((support_child X A (not|T))
	(!)
	(support_child X A T))
((support_child X A (or L|B))
	(!)
	(support_children X A L)
	(support_child X A (or|B)))
((support_child _ _ (or))
	(!))
((support_child X A (? L))
	(!)
	(prolog_children X A L))
((support_child X A (H|T))
	(prolog_meta H)
	(!)
	(escape stdout (p Warning - subgoal (H|T) of clause X arity A))
	(pp)
	(escape stdout (p Fril prolog meta predicate inside support clause))
	(pp)
	(escape stdout (p Behaviour may be unpredictable -)
		       (p '' avoid by embedding in a (? ... )))
	(pp)
	(escape stdout (p However - ignoring for the present))
	(pp)
	(prolog_child X A (H|T)))
((support_child _ _ (H|_))
	(sys H)
	(!))
((support_child X A (H|T))
	(dict temp_child)
	(temp_child X A H A1)
	(arb_length T A1)
	(!))
((support_child X A (H|T))
	(arb_length T A1)
	(addcl ((temp_child X A H A1)))
	(!))
((support_child _ _ _))

((prolog_children X A ((H|T)|B))
	(prolog_child X A (H|T))
	(!)
	(prolog_children X A B))
((prolog_children X A ()))

((prolog_child X _ (H|_))
	(stricteq X H)
	(!))
((prolog_child X A (neg T))
	(!)
	(prolog_child X A T))
((prolog_child X A (negg|T))
	(!)
	(prolog_child X A T))
((prolog_child X A (orr L|B))
	(!)
	(prolog_children X A L)
	(prolog_child X A (orr|B)))
((prolog_child _ _ (orr))
	(!))
((prolog_child X A (? L))
	(!)
	(prolog_children X A L))
((prolog_child X A (supp_query L _))
	(!)
	(support_children X A L))
((prolog_child X A (findall _ L _))
	(!)
	(prolog_children X A L))
((prolog_child X A (isall _ _|L))
	(!)
	(prolog_children X A L))
((prolog_child X A (forall L G))
	(!)
	(prolog_children X A L)
	(prolog_children X A G))
((prolog_child X A (if G T E))
	(!)
	(prolog_child X A G)
	(prolog_children X A T)
	(prolog_children X A E))
((prolog_child X A (snips|T))
	(!)
	(prolog_child X A T))
((prolog_child X A (qh T))
	(!)
	(prolog_children X A T))
((prolog_child X A (oh T))
	(!)
	(prolog_children X A T))
((prolog_child X A (wh (_|T)))
	(!)
	(prolog_children X A T))
((prolog_child X A (qs T))
	(!)
	(support_children X A T))
((prolog_child X A (os T))
	(!)
	(support_children X A T))
((prolog_child X A (ws (_|T)))
	(!)
	(support_children X A T))
((prolog_child X A (H|T))
	(support_meta H)
	(!)
	(escape stdout (p Warning - subgoal (H|T) of clause X arity A))
	(pp)
	(escape stdout (p Fril support meta predicate inside prolog clause))
	(pp)
	(escape stdout (p Error message likely when subgoal is called))
	(pp)
	(escape stdout (p However - ignoring for the present))
	(pp)
	(support_child X A (H|T)))
((prolog_child _ _ (H|_))
	(sys H)
	(!))
((prolog_child X A (H|T))
	(dict temp_child)
	(temp_child X A H A1)
	(arb_length T A1)
	(!))
((prolog_child X A (H|T))
	(arb_length T A1)
	(addcl ((temp_child X A H A1)))
	(!))
((prolog_child _ _ _))

((type X relation)
	(rdict X)
	(!))
((type X dempster)
	(ddict X)
	(!))
((type X clause))

((arb_length L N)
	(arb_len L 0 N)
	(!))

((arb_len X _ n)
	(var X)
	(!))
((arb_len (_|T) K N)
	(sum K 1 M)
	(arb_len T M N))
((arb_len () N N))

((assert_arity X T A N)
	(dict temp_arity)
	(delcl ((temp_arity X T A M)))
	(!)
	(sum M 1 N)
	(addcl ((temp_arity X T A N))))
((assert_arity X T A 1)
	(addcl ((temp_arity X T A 1))))

((assert_parents X A)
	(temp_child X A H A1)
	(assert_parent H A1 X A)
	(assert_child_name H A1)
	(fail))
((assert_parents _ _))

((assert_parent H A1 X A)
	(dict temp_parent)
	(temp_parent H A1 X A)
	(!))
((assert_parent H A1 X A)
	(addcl ((temp_parent H A1 X A))))

((assert_child_name H A1)
	(dict temp_child_name)
	(temp_child_name H A1)
	(!))
((assert_child_name H A1)
	(addcl ((temp_child_name H A1))))

((assert_children)
	(temp_child_name H A1)
	(findall (X A) ((temp_parent H A1 X A)) L)
	(addcl ((child H A1 L)))
	(fail))
((assert_children))

((tidy_parent_child)
	(parent X _ _ N _)
	(negg child X _ _)
	(addcl ((child X N top_level)))
	(fail))
((tidy_parent_child)
	(child X N _)
	(negg parent X _ _ _ _)
	(addcl ((parent X undefined unknown N unknown)))
	(fail))
((tidy_parent_child))

/* convert_dict is used in "collect_subgoals" below */

((convert_dict)
	(kill temp_diction)
	(temp_diction1 X)
	(addcl ((temp_diction X)))
	(fail))
((convert_dict)
	(dict temp_diction1)
	(kill temp_diction1))

/* convert_diction (together with convert_diction1) is used in
	"diction_parents" below                                                 */

((convert_diction)
	(dict diction)
	(diction X _ _ _ _)
	(convert_diction1 X)
	(fail))
((convert_diction))

((convert_diction1 X)
	(dict temp_diction)
	(temp_diction X)
	(!))
((convert_diction1 X)
	(addcl ((temp_diction X))))

((assert_temp_diction1)
	(temp_child _ _ H _)
	(assert_temp_diction2 H)
	(fail))
((assert_temp_diction1))

((assert_temp_diction2 H)
	(dict temp_diction1)
	(temp_diction1 H)
	(!))
((assert_temp_diction2 H)
	(addcl ((temp_diction1 H))))

/* Three top_level verification predicates start here
	collect_subs verifies parent and child relations of the current goal
	diction_parents verifies parent and child relations of all
		predicates (re)consulted as determined by predicate "diction"
	find_parents verifies parent and child relations of all predicates
		in the database - should therfore be used with circumspection

   All three top_level predicates call "find_parent"                    */

((collect_subs (H|T))
	(kill child) (kill parent)
	(kill temp_parent) (kill temp_child_name)
	(kill temp_diction) (kill temp_diction1)
	(def_rel parent 5) (def_rel child 3)
	(addcl ((temp_diction H)))
	(collect_subgoals))
((collect_subs _)
	(assert_children)
	(tidy_parent_child)
	(kill temp_parent) (kill temp_child_name)
	(kill temp_diction) (kill temp_diction1)
	(kb_garbage))

((collect_subgoals)
	(temp_diction X)
	(find_parent X)
	(fail))
((collect_subgoals)
	(convert_dict)
	(!)
	(collect_subgoals))

((diction_parents)
	(kill child) (kill parent)
	(kill temp_parent) (kill temp_child_name)
	(kill temp_diction)
	(convert_diction)
	(def_rel parent 5) (def_rel child 3)
	(temp_diction X)
	(find_parent X)
	(fail))
((diction_parents)
	(assert_children)
	(tidy_parent_child)
	(kill temp_parent) (kill temp_child_name)
	(kill temp_diction) (kill temp_diction1)
	(kb_garbage))

((find_parents)
	(kill child) (kill parent)
	(kill temp_parent) (kill temp_child_name)
	(def_rel parent 5) (def_rel child 3)
	(dict X)
	(find_parent X)
	(fail))
((find_parents)
	(assert_children)
	(tidy_parent_child)
	(kill temp_parent) (kill temp_child_name)
	(kill temp_diction1)
	(kb_garbage))

((find_parent X)
	(kill temp_child) (kill temp_arity)
	(cl ((X|Y)|Z))
	(type X T)
	(arb_length Y A)
	(assert_arity X T A N)
	(support_children X A Z)
	(fail))
((find_parent X)
	(dict temp_arity)
	(temp_arity X T A N)
	(findall (H A1) ((temp_child X A H A1)) L)
	(addcl ((parent X T A N L)))
	(assert_parents X A)
	(fail))
((find_parent X)
	(assert_temp_diction1)
	(kill temp_child) (kill temp_arity)
	(kb_garbage))

