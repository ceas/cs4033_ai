/* *******************************************************
File: fs_gram.frl	     Version: 4.1    Date: 25/3/88

Copyright: Equipu-AIR, Ltd.

Summary: This file constitutes a package for Definite
Clause Grammar (DCG) translation and interpretation.
"grammar" is the basic user predicate for the translation
of grammar rules into corresponding Fril prolog or Fril
support logic clauses and adds them to the database.  A
dictionary of translated grammar rules is kept in a fact
clause "gdict".  "translate" translates the contents of a
file of grammar rules and writes the translated clauses as
well as the dictionary predicate "gdict" to a file
specified by the user.	"savegram" lists "gdict" and all
the translated grammar rules currently defined in the
database, as indicated by "gdict", to a file specified by
the user.  "phrase", "parse", "parse_supp", "trygram" and
"try_supp" are grammar interpretation utilities defined
below.	"reader" is a utility for reading text typed at
the keyboard (standard input) into a list.  "listg"
displays the definitions of translated grammar rules to
the standard input (screen) or to a specified file, with
pretty printing if required.

Goal (grammar +(<grammar_rule1> ... <grammar_rulen>))
translates the list of grammar rules into clauses which
are added to the database.  The names of the grammar rules
are recorded in a dictionary of fact clauses of the form
((gdict <name>)), one clause for each name, and predicate
"gdict" is also added to the database.

Goal (translate +<filename>) prompts the user for the name
of an output file, say <outfile>, and then invokes the
goal (translate +<filename> +<outfile>).

Goal (translate +<filename> +<outfile>) translates any
grammar rules found in file <filename> and writes the
translated clauses to file <outfile>.  The corresponding
dictionary predicate "gdict" is also written to
<outfile>.  None of the grammar rules are added to the
database, and any other translated grammar rules and
"gdict" dictionary which are in the database are left
unaffected. Note also that any normal Fril syntax rules
in <filename> are added to the database just as though the
file had been reloaded, rather than being written to
<outfile>.

Goal (savegram +<stream>) executes bip "listfile" on each
of the translated grammar rules currently in the database
as indicated by "gdict", saving to <stream>.  "gdict" is
also displayed on <stream>.

Goal (listg +<arg>) executes "listf" on the name(s) of
translated grammar rules as indicated by "gdict".  If
<arg> is an uninstantiated variable or the symbol "all",
then all the grammar clauses are displayed, otherwise the
clauses for the named predicate are displayed.	The
clauses will be pretty printed if the switch status is
"on".

Goal (listg +<stream> +<arg>) is similar to (listg +<arg>)
above, except that the clauses are displayed to <stream>.

Goal (phrase +(<sentence> <arg1> ... <argn>) +<list>)
executes the goal (<sentence> <arg1> ... <argn> <list> ())
which parses the list of "words" <list> according to the
"grammar" <sentence>.  "phrase" provides a primitive
mechanism for the interpretation of translated Fril
definite clause grammar rules.	"phrase" is resatisfiable
only to the extent that <sentence> is resatisfiable.

Goal (parse +<sentence>) reads in a sequence of symbols
typed at the keyboard (standard input) using USER
predicate "reader", and parses this according to the
definite clause grammar rule named <sentence>, which is
assumed to take at least one argument.	It is assumed that
the last argument of grammar rule <sentence> becomes
instantiated to the corresponding parse tree if the parse
succeeds, and it is printed at the standard output using
USER predicate "pt".  The user is prompted to try for
additional parses of the current sequence and/or parses of
further sequences.  "parse" provides a basic  mechanism
for the interpretation of translated Fril definite clause
grammar rules.	"parse" is not resatisfiable.

Goal (parse_supp +<sentence>) is similar to "parse" above,
except that name <sentence> defines a Support Logic
Grammar rather than the Definite Clause Grammar as assumed
by "parse".  If the parse succeeds, the corresponding
parse tree is printed out using "pt" together with its
computed support.  The user is prompted to try for
additional parses of the current sequence and/or parses of
further sequences.  "parse_supp" provides a basic
mechanism for the interpretation of translated Fril
support logic grammar rules.  "parse_supp" is not
resatisfiable.

Goal (reader -<list>) reads in a sequence of characters
typed at the standard input (keyboard) and converts this
to a list of symbols <list>.  The sequence is terminated
by a full stop "." followed immediately by <Enter>.
"reader" recognises each of the following single
characters as individual words:  !, #, $, &, *, +, -,
/, :, ;, <, =, >, ?, @, \, ^, _, `, |, ~, �.   "reader"
also recognises integers and decimal numbers. "reader" is
not resatisfiable.

Goal (trygram +<int>) assumes that a clause fact ((words
+<list>)) is defined where <list> is a list of terminal
symbols of a grammar, and that the Definite Clause Grammar
rule with head ("sentence" <arg>) is also defined.  The
goal attempts to generate legal sentences of the grammar
by parsing subsets of the list of words <list>, and
printing corresponding parse trees using USER predicate
"ppt".  Since the number of possible subsets grows
exponentially with the length of <list>, some
experimentation is needed.  This is made possible to a
limited extent by instantiating the argument <int> to an
integer which then determines how many places the <list>
is to be rotated.  On completion of a successful parse,
the user is prompted to search for further subsets of
<list> which parse.  Selection of the option <index>
allows the user to skip to the <index>th subset before
attempting to parse.  "trygram" is not resatisfiable.

Goal (trysupp +<int>) is similar to "trygram" above,
except that "sentence" defines a Support Logic Grammar
rather than the Definite Clause Grammar which trygram
assumes.  If a parse succeeds, the corresponding parse
tree is printed out using "ppt", together with its
computed support.  "trysupp" is not resatisfiable.

Predicate "gdict" is created or extended during the
translation of grammar rules using predicate "grammar".
Fact clauses of the form ((gdict <gram_name>)) are added
to the database - one clause for each different grammar
rule name.

USERs: translate/1, translate/2, savegram/1, grammar/1,
       phrase/1, parse/1, parse_supp/1, reader/1,
       trygram/1, try_supp/1, gdict/1, listg/1, listg/2

EXPORTs: none

INTERNALs: translate1/1, translate2/2, savegram1/1,
	   grammar1/2, stack_dict/2, gram_record/1,
	   gram_translate/2, gram_translate1/3,
	   gram_lhs/4, gram_rhs/4, gram_rhs1/4,
	   gram_disj/4, gram_conj/3, gram_flatten/2,
	   gram_flatten1/3, gram_go/2, senrep/3,
	   gen_newvarlist/2, read_in/3, proc_tok/5,
	   convert_list/2, convert_code/2, new_token/1,
	   count/2, trygram1/1, trysupp1/1

TEMPORARYs: "$gram_out_file"/1, "$gdict1"/1, counter/1,
	    skipcounter/1

IMPORTs:
File fs_lspr1.frl: append/3, length/2, reverse/2
File fs_lspr2.frl: islist/1
File fs_lspr3.frl: rotate/3, subset/2
File fs_prnt.frl: pt/n, ppt/n
File fs_swtch.frl: switch/1
File fs_list.frl: listf/n
File fs_exten.frl: addfrl_extension/2
File <application_file>: words/1

******************************************************* */

reload fs_lspr2
reload fs_lspr3
reload fs_prnt
reload fs_swtch
reload fs_list
reload fs_exten

((translate F F2)
  (addfrl_extension F F1)
  (orr ((negg con F1))
       ((negg exists F1))
	   ((negg con F2)))
  (!)
  (translate1 F1))
((translate F1 F2)
  (translate2 F1 F2))

((translate F)
  (addfrl_extension F F1)
  (translate1 F1))

((translate1 F1)
  (negg con F1)
  (!)
  (pp 'First argument must be instantiated to a constant')
  (pp '  - this should be the name of a file of grammar rules')
  (fail))
((translate1 F1)
  (negg exists F1)
  (!)
  (p File F1 does not exist)
  (pp)
  (fail))
((translate1 F1)
  (p Enter name of output file ': ')
  (r F2)
  (if (negg con F2) ((pp 'Illegal file name - try again')
		     (translate F1))
		    ((translate2 F1 F2))))

((translate2 F stdout)
  (!)
  (translate3 F stdout)
  (pp)
  (pp 'Translation completed - listing of grammar dictionary "gdict" :')
  (list gdict)
  (stack_gdict gdict "$gdict1")
  (kill "$gram_out_file"))
((translate2 F1 F2)
  (addfrl_extension F2 F3)
  (if (exists F3)
      ((p Output file already F3 already exists)(pp)
	   (p Append to file (a) or overwrite file (o) ': ')
	   (flush stdin)
	   (getb stdin X)
	   (if (eq X 111) ((p Overwriting F3)(pp)(create F3))
	                  ((p Appending to F3)(pp)(create_a F3)) ))
      ((create F3)) )
  (translate3 F1 F3)
  (close F3)
  (pp)
  (pp 'Translation completed - saving grammar dictionary "gdict"')
  (listfile F3 gdict)
  (stack_gdict gdict "$gdict1")
  (kill "$gram_out_file"))

((translate3 F1 F3)
  (kill "$gram_out_file")
  (addcl (("$gram_out_file" F3)))
  (stack_gdict "$gdict1" gdict)
  (load F1))

((savegram F2)
  (addfrl_extension F2 F3)
  (savegram1 F3))

((savegram1 F3)
  (gdict X)
  (listfile F3 X)
  (fail))
((savegram1 F3)
  (listfile F3 gdict))

((grammar (X : S|B))
  (!)
  (grammar1 X S)
  (grammar B))
((grammar (X|B))
  (!)
  (grammar1 X prolog)
  (grammar B))
((grammar ()))

((grammar1 ((H|T)|B) S)
  (cdict "$gram_out_file")
  (!)
  ("$gram_out_file" F)
  (gram_record H)
  (gram_translate ((H|T)|B) P)
  (if (stricteq S prolog)
		  ((wq F P))
		  ((wq F P : S)))
  (write F)
  (if (eq F stdout) () ((p '.')) ))
((grammar1 ((H|T)|B) S)
  (gram_record H)
  (gram_translate ((H|T)|B) P)
  (if (stricteq S prolog)
		  ((addcl P))
		  ((addcl P : S))))

((stack_gdict X Y)
  (kill X)
  (cdict Y)
  (!)
  (forall ((Y N))
		  ((delcl ((Y N)))
		   (addcl ((X N))))))
((stack_gdict _ _))

((gram_record H)
  (cdict gdict)
  (gdict H)
  (!))
((gram_record H)
  (kill H)
  (addcl ((gdict H))))

((gram_translate (X '-->'|Z) (P|Q))
  (gram_translate1 X Z (P|Q)))
((gram_translate (X (Y) '-->'|Z) (P|Q))
  (gram_translate1 (X Y) Z (P|Q)))

((gram_translate1 X Z (P|Q))
  (gram_lhs X A B P)
  (gram_rhs Z A B R)
  (gram_flatten R Q))

((gram_lhs X _ _ _)
  (var X)
  (!)
  (fail))
((gram_lhs (S T) A B (X|P))
  (islist T)
  (eq S (X|Y))
  (!)
  (append Y (A C) P)
  (append T A C))
((gram_lhs (X|Y) A B (X|P))
  (append Y (A B) P))

((gram_rhs () A A ()))
((gram_rhs ((? ())|R) A B P)
  (!)
  (gram_rhs R A B P))
((gram_rhs ((? (H|T))|R) A B P)
  (!)
  (gram_rhs1 (? H) A C P1)
  (gram_rhs ((? T)|R) C B P2)
  (gram_conj P1 P2 P))
((gram_rhs ((H|T)|R) A B P)
  (!)
  (gram_rhs1 (H|T) A C P1)
  (gram_rhs R C B P2)
  (gram_conj P1 P2 P))

((gram_rhs1 (orr X Y) A B (orr P Q))
  (!)
  (gram_disj X A B P)
  (gram_disj Y A B Q))
((gram_rhs1 (? P) A A P)
  (!))
((gram_rhs1 (!) A A (!))
  (!))
((gram_rhs1 (S) A B ())
  (islist S)
  (!)
  (append S B A))
((gram_rhs1 (X|Y) A B (X|P))
  (append Y (A B) P))
((gram_rhs1 () A A ()))

((gram_disj X A B P)
  (gram_rhs X C B Q)
  (orr ((var C) (eq C B) (!) (eq A C) (eq P Q))
       ((eq P ((eq A C)|Q)))))

((gram_conj () P P)
  (!))
((gram_conj P () (P))
  (!))
((gram_conj P Q (P|Q)))

((gram_flatten A A)
  (var A)
  (!))
((gram_flatten ((H|T)|B) R)
  (!)
  (gram_flatten1 (H|T) R P)
  (gram_flatten B P))
((gram_flatten A A))

((gram_flatten1 A (A|B) B)
  (var A)
  (!))
((gram_flatten1 ((H|T)|B) C R)
  (!)
  (gram_flatten1 (H|T) C S)
  (gram_flatten1 B S R))
((gram_flatten1 (H|T) ((H|T)|R) R))

((listg X)
  (if (stricteq X all) () ((eq X Y)))
  (cdict gdict)
  (gdict Y)
  (listf Y)
  (fail))
((listg _))
((listg F X)
  (if (stricteq X all) () ((eq X Y)))
  (cdict gdict)
  (gdict Y)
  (listf F Y)
  (fail))
((listg _ _))

((phrase (P|A) W)
  (append A (W ()) N)
  (P|N))

((parse T)
  (flush stdin)
  (repeat)
  (kb_garbage)
  (gram_go T dcg)
  (pp)
  (p 'Another string or sentence ? (y/n) ')
  (r X)
  (pp)
  (eq X n))

((parse_supp T)
  (flush stdin)
  (repeat)
  (kb_garbage)
  (gram_go T supp)
  (pp)
  (p 'Another string or sentence ? (y/n) ')
  (r X)
  (pp)
  (eq X n))

((gram_go T Z)
  (pp)
  (p 'Enter string or sentence: ')
  (pp)
  (reader S)
  (pp)
  (p S)
  (pp)
  (senrep S T Z)
  (!))

((senrep S T Z)
  (snips cl ((T|A)|_))
  (length A N)
  (sum 3 M N)
  (gen_newvarlist M V)
  (append V (P S ()) W)
  (if (eq Z dcg) ((T|W)
		  (pp) (pt P))
	((if (eq Z supp)
		 ((errm n)
		  (supp_query ((T|W)) SUPP)
		  (pp) (pt P) (p : SUPP) (errm y))
		 ((fail)))))
  (pp)
  (pp)
  (p 'More ? (y/n) ')
  (r X)
  (pp)
  (eq X n))
((senrep _ _ _)
  (p '  No more parses.')
  (pp))

((gen_newvarlist N ())
  (less N 1)
  (!))
((gen_newvarlist N (H|T))
  (sum M 1 N)
  (gen_newvarlist M T))

((reader S)
  (pp)
  (read_in stdin () S)
  (!))

((read_in F L1 L2)
	(intok F X)
	(name N X)
	(proc_tok F N X L1 L2))

((proc_tok _ (46) '.' L1 L2)		/* '.' terminates input */
	(!)
	(reverse L1 L2))
((proc_tok F (_) X L1 L2)			/* single character token */
	(!)
	(read_in F (X|L1) L2))
((proc_tok F (H|T) _ L1 L2)			/* punctuation characters need separating */
	(convert_code H H2)
	(new_token H2)
	(!)
	(charof C H2)
	(convert_list T T2)
	(name T2 X)
	(proc_tok F T X (C|L1) L2))
((proc_tok F _ X L1 L2)				/* other tokens accepted directly */
	(read_in F (X|L1) L2))

/* these two conversions are required 'cos getb and name can
   generate negative ascii values */

((convert_list () ()))
((convert_list (H|T) (H2|T2) )
	(convert_code H H2)
	(convert_list T T2))

((convert_code H H2)
	(less H 0)
	(!)
	(sum H 256 H2))
((convert_code H H))

(new_token (33) (35) (36) (37) (38) (42) (43) (45) (47)
	   (58) (59) (60) (61) (62) (63) (64) (92) (94) (95) (96)
		   (124) (126) (156) (170) )

((count X COUNTER)
  (delcl ((COUNTER Y)))
  (sum Y 1 X)
  (addcl ((COUNTER X))))

((trygram N)
  (kill skipcounter)
  (kill counter)
  (addcl ((skipcounter 1)))
  (addcl ((counter 0)))
  (words X)
  (rotate X N Y)
  (trygram1 Y))

((trygram1 X)
  (p Dictionary ordered as X)
  (pp) (pp)
  (subset X Y)
  (skipcounter J)
  (count I counter)
  (eq I J)
  (count K skipcounter)
  (remainder K 50.0 W)
  (if (eq W 0) ((p K ' ')) ())
  (kb_garbage)
  (phrase (sentence Z) Y)
  (pp)
  (p I Y)
  (pp)
  (ppt Z)
  (pp)
  (p 'next ? (n, y or <index>) ')
  (r R)
  (if (eq R n) ()
	((if (int R) ((kill skipcounter)
			(addcl ((skipcounter R)))
			(fail))
		     ((fail))))))
((trygram1)
  (pp)
  (pp 'no more parses'))

((trysupp N)
  (errm n)
  (kill skipcounter)
  (kill counter)
  (addcl ((skipcounter 1)))
  (addcl ((counter 0)))
  (words X)
  (rotate X N Y)
  (trysupp1 Y)
  (errm y))

((trysupp1 X)
  (p Dictionary ordered as X)
  (pp) (pp)
  (subset X Y)
  (skipcounter J)
  (count I counter)
  (eq I J)
  (count K skipcounter)
  (remainder K 50.0 W)
  (if (eq W 0) ((p K ' ')) ())
  (kb_garbage)
  (supp_query ((sentence Z Y ())) S)
  (pp)
  (p I Y)
  (pp) (pp)
  (pt Z) (p : S)
  (pp)
  (pp)
  (p 'next ? (n, y or <index>) ')
  (r R)
  (if (eq R n) ()
	((if (int R) ((kill skipcounter)
			(addcl ((skipcounter R)))
			(fail))
		     ((fail))))))
((trysupp1)
  (pp)
  (pp 'no more support parses'))

