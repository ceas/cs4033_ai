# Homework #2

This directory contains all materials required for submission of HW2.

## Requirements

The original HW2 document `20CS4033SS19_Assignment2_BlocksWorld.docx` can be found on blackboard, but I have also copied it to this directory.

HW2 requires 4 algorithms written in FRIL:

- Depth First Search
- Breadth First Search
- Best First Search
- Heuristic (A*) Search

Each needs to be written for a blocks world.